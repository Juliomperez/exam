package com.example.exam.Fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.example.exam.MainActivity
import com.example.exam.R

class FirstFragment : Fragment(),View.OnClickListener {

    lateinit var etEmail:EditText
    lateinit var etPassword:EditText
    lateinit var btnLogin:Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_first, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        etEmail=view.findViewById(R.id.etEmail)
        etPassword=view.findViewById(R.id.etPassword)
        btnLogin=view.findViewById(R.id.btnLogin)
        btnLogin.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        var textoEmail:String=etEmail.text.toString()
        var textoPassword:String=etPassword.text.toString()

        if (textoEmail=="manolo" && textoPassword=="123456"){
            Toast.makeText(requireActivity(),"Login Successful",Toast.LENGTH_LONG).show()
        }

        else{
            Toast.makeText(requireActivity(),"Login Failed",Toast.LENGTH_LONG).show()
        }
    }
}